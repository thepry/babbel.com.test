# Install

```
rake db:create
rake db:migrate

rails s
```

`/accounts/sign_up` - registration path
`accounts/:id/emails` - list of accounts emails

`rake routes` - other routes

# Testing

```
rake db:create RAILS_ENV=test
rake db:migrate RAILS_ENV=test

rspec
```

# backend.challenge

This repository holds the backend development challenge for applicants of Babbel.

## The application

The application in this repository is simplistic. It consists of one model `Account`. The `Account` model is also baked by [devise](https://github.com/plataformatec/devise).

## The task

The customer service department wants to keep track of all the email addresses a user has had in its history as a Babbel customer. The previous email addresses should be persisted.

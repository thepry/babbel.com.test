module Accounts
  class AccountEmailsController < ApplicationController
    add_views_path :account_emails

    def index
      @emails = AccountEmail.where(account_id: params[:account_id])
    end
  end
end

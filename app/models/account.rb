class Account < ActiveRecord::Base
  devise :confirmable, :database_authenticatable, :lockable,
         :recoverable, :registerable, :rememberable, :validatable

  has_many :account_emails

  def confirm(args = {})
    transaction do
      super.tap do |res|
        account_emails.create!(email: email) if res
      end
    end
  end
end

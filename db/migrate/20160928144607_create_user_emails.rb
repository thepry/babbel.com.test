class CreateUserEmails < ActiveRecord::Migration
  def change
    create_table :account_emails do |t|
      t.integer :account_id, null: false
      t.string :email, null: false

      t.timestamps
    end
  end
end

require 'rails_helper'

describe 'Account' do
  context 'new user' do
    before do
      visit new_account_registration_path
      fill_in 'Email', with: 'test@test.test'
      fill_in 'Password', with: 'secret'
      fill_in 'Password confirmation', with: 'secret'
      click_button 'Sign up'
    end

    it "doesn't store account email untill it's confirmed" do
      visit account_account_emails_path(Account.last)
      expect(page).to_not have_text('test@test.test')
    end

    it 'stores account email after confirmation' do
      visit links_in_last_email.first
      visit account_account_emails_path(Account.last)
      expect(page).to have_text('test@test.test')
    end
  end

  context 'existing user' do
    let(:account) { create :account }

    before do
      visit new_account_session_path
      fill_in 'Email', with: account.email
      fill_in 'Password', with: 'secret'
      click_button 'Log in'
    end

    describe 'email change' do
      let(:new_email) { Faker::Internet.email }

      before do
        visit edit_account_registration_path
        fill_in 'Email', with: new_email
        fill_in 'Current password', with: 'secret'
        click_button 'Update'
      end

      it "doesn't store account's new email untill it's confirmed" do
        visit account_account_emails_path(Account.last)
        expect(page).to_not have_text new_email
      end

      it "stores account's new email after it's confirmed" do
        visit links_in_last_email.first
        visit account_account_emails_path(Account.last)
        expect(page).to have_text new_email
      end
    end
  end
end

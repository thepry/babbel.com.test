require 'rails_helper'

describe Account do
  let(:account) { create :account }

  it "creates an AccountEmail after email confirmation" do
    new_email = Faker::Internet.email
    account.update!(email: new_email)
    expect(account.confirm).to be true
    expect(account.account_emails.pluck(:email)).to include(new_email)
  end
end

module EmailHelpers
  def links_in_last_email
    URI.extract(emails_sent.last.body.raw_source, %w(http https))
  end

  def emails_sent
    ActionMailer::Base.deliveries
  end
end
